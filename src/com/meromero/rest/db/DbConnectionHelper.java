package com.meromero.rest.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface to handle database creation and connections
 *
 * @author Santos R. Cash
 */
public interface DbConnectionHelper {

	public Connection getConnection() throws SQLException;

	public void closeConnection(Connection connection);

}