package com.meromero.rest.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * SQLite helper class that handles database creation and connections
 *
 * @author Santos R. Cash
 */
public class DbConnectionHelperSQLiteImpl implements DbConnectionHelper {

	private String driver;
	private String connURL;
	private static Connection conn;

	public DbConnectionHelperSQLiteImpl() {

		conn = null;
		
		//////////////////////////////////////////////////////////////////
		// TODO:
		//	These shouldn't be hard coded...create a properties file
		//		and access via a ResourceBundle (or something like that).
		//////////////////////////////////////////////////////////////////
		driver = "org.sqlite.JDBC";
		connURL = "jdbc:sqlite:rest.db";

		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			// The driver was not located
			e.printStackTrace();
		}

		setDb();

	}

	/**
	 * If the database doesn't exist, create one and populate it.
	 */
	private synchronized void setDb() {

		try {
			conn = getConnection();

			// Create table if it isn't already present
			String sqlQuery = "CREATE TABLE IF NOT EXISTS sum (total INTEGER NOT NULL)";

			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlQuery);

			ResultSet rs = stmt.executeQuery("SELECT total FROM sum;");

			if(!rs.isBeforeFirst()) {
				sqlQuery = "INSERT INTO sum (total) VALUES (0);";
				stmt.executeUpdate(sqlQuery);
			}

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn);
		}

	}

	/**
	 * Retrieve a connection to the database
	 *
	 * @return	a {@link Connection} object to access the database
	 */
	@Override
	public Connection getConnection() throws SQLException {

		if((conn == null) || conn.isClosed()) {
			conn = DriverManager.getConnection(connURL);
		}

		return conn;

	}

	/**
	 * Close the specified database connection
	 *
	 * @param	connection	a {@link Connection} object to close
	 */
	@Override
	public void closeConnection(Connection connection) {

		try {
			if(connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}