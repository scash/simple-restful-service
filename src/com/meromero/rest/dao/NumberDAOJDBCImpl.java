package com.meromero.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.meromero.rest.db.DbConnectionHelperSQLiteImpl;
import com.meromero.rest.model.Numero;

/**
 * A DAO instance that handles database access/manipulation through JDBC
 *
 * @author Santos R. Cash
 */
public class NumberDAOJDBCImpl implements NumberDAO {

	private Numero num;
	private DbConnectionHelperSQLiteImpl dbConn;

	public NumberDAOJDBCImpl() {

		num = new Numero();
		dbConn = new DbConnectionHelperSQLiteImpl();

	}

	/**
	 * Retrieve sum of numbers
	 *
	 * @return	a {@link Numero} object representing the current sum
	 */
	@Override
	public Numero getDbVal() {

		Connection conn = null;

		try {
			conn = dbConn.getConnection();

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT total FROM sum;");

			if(rs.next()) {
				num.setNumber(rs.getInt("total"));
			}

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbConn.closeConnection(conn);
		}

		return num;
	}

	/**
	 * Add a value to the existing sum
	 *
	 * @param	value	a number to add to the current sum
	 */
	@Override
	public void updateDbVal(int value) {

		Connection conn = null;

		try {
			conn = dbConn.getConnection();

			PreparedStatement ps = conn.prepareStatement("UPDATE sum set total = total + ?;");
			ps.setInt(1, value);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbConn.closeConnection(conn);
		}

	}

	/**
	 * Delete stored data, reset to zero
	 */
	@Override
	public void deleteDbVal() {

		Connection conn = null;

		try {
			conn = dbConn.getConnection();

			Statement stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE sum set total = 0;");
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbConn.closeConnection(conn);
		}

	}

}