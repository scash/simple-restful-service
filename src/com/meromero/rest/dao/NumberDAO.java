package com.meromero.rest.dao;

import com.meromero.rest.model.Numero;

/**
 * A DAO interface to handle database manipulation
 *
 * @author Santos R. Cash
 */
public interface NumberDAO {

	public Numero getDbVal();

	public void updateDbVal(int value);

	public void deleteDbVal();

}