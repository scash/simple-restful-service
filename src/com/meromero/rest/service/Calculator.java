package com.meromero.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.meromero.rest.dao.NumberDAOJDBCImpl;

/**
 * A RESTful web service resource that handles simple numeric arithmetic
 *
 * @author Santos R. Cash
 */
@Path("/values")
@Produces(MediaType.TEXT_PLAIN)
public class Calculator {

	private NumberDAOJDBCImpl dao = new NumberDAOJDBCImpl();

	/**
	 * Retrieve sum of numbers
	 *
	 * @return	a number representing the current sum
	 */
	@Path("/sum")
	@GET
	public int getValue() {

		return dao.getDbVal().getNumber();

	}

	/**
	 * Add a value to the existing sum
	 *
	 * @param	value	a number to add to the current sum
	 * @return	a {@link Response} object to handle output
	 */
	@POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addValue(@FormParam("value") int value) {

		dao.updateDbVal(value);

		return Response.ok("ok").build();

	}

	/**
	 * Delete stored data, reset to zero
	 *
	 * @return	a {@link Response} object to handle output
	 */
	@DELETE
	public Response deleteValue() {

		dao.deleteDbVal();

		return Response.ok("ok").build();

	}

}