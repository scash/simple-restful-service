package com.meromero.rest.model;

/**
 * An object representation of the data in the database
 *
 * @author Santos R. Cash
 */
public class Numero {

	private int number;

	public Numero() {
		number = 0;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}