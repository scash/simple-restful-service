This is a very simple RESTful web service that accepts a set of numbers and calculates the sum of the numbers. This service has a single resource called *values*. POSTing a value to the *values* resource adds the value to the service. DELETEing values will clear all the values from the service. GETting values/sum will return the sum of all the values in the service.

This service handles persistence through a DAO layer via JDBC. This service is packaged with a SQLite JDBC driver, but if you'd like to use a different driver, it could be accomplished with a few tweaks.

--------------------
## Technologies
1. Java

2. SQLite JDBC 3.7.2 ([more info](https://bitbucket.org/xerial/sqlite-jdbc))

3. Jersey 2.10 (JAX-RS implementation)

4. Tomcat 8 (but you can leverage any Servlet 3.0 container)

## Deployment

As stated above, this code can run on any Servlet 3.0 container. Set that up however you'd like, per the deployment instructions for that container. The simplest approach might be to deploy from your preferred IDE, as most Java IDEs now allow "servers" to be run from within.

## Usage
Assuming the service is set to run from *localhost:8080* you can use **curl** to test service interaction as follow:

To get the current sum
```sh
curl http://localhost:8080/api/values/sum
```
To add a value of 10 to the existing sum
```sh
curl -X POST http://localhost:8080/api/values --data "value=10"
```
To delete the sum
```sh
curl -X DELETE http://localhost:8080/api/values
```